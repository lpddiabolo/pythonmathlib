import matplotlib.pyplot as plt
import numpy as np
import sympy

#深入理解透视矩阵 https://www.zhyingkun.com/markdown/perspective/
#对比了闫令琪老师在 GAMES101 给出的推导思路 和 《3D 游戏与计算机图形学中的数学方法》书中的推导思路

'''使用 GAMES101 给出的推导思路，n和f都是带有符号的值(例如 n = -3, f= -400)
MatPersp = 
|   2n/(r-l)    0           0           0           |
|   0           2n/(t-b)    0           0           |
|   0           0           n+f/f-n     -2nf/f-n    |
|   0           0           1           0           |
'''
# far = -400 #远平面
# near = -3  #近平面
# #Opengl model space 和eye space 是右手坐标系 Z值与摄像机方向相反
# #eye space Z：从近平面到远平面
# Zeye = np.linspace(near,far, 100, endpoint = True)


# #下面是Zeye通过透视矩阵变换后得到ndc space的Zndc值
# #clip space Z：透视投影
# Zclip = (far + near)* Zeye /(far - near) - 2 * far * near / (far - near) 
# #ndc space Z：齐次除法（透视除法) 除W
# Zndc =  Zclip / Zeye



'''
OpenGL Projection Matrix http://www.songho.ca/opengl/gl_projectionmatrix.html
使用书推导出的变化矩阵 这里n和f是取绝对值的非负数 推导结果和上面网站一样
MatPersp = 
|   2n/(r-l)    0           0           0           |
|   0           2n/(t-b)    0           0           |
|   0           0           -(n+f)/f-n  -2nf/f-n    |
|   0           0           -1           0          |
'''

far = 400 #远平面
near = 3  #近平面

#Opengl model space 和eye space 是右手坐标系 Z值与摄像机方向相反
#eye space Z：从近平面到远平面
Zeye = np.linspace( -near, -far, 100, endpoint = True)

#下面是Zeye通过透视矩阵变换后得到ndc space的Zndc值
#clip space Z：透视投影
Zclip = -(far + near)* Zeye /(far - near) - 2 * far * near / (far - near) 
#ndc space Z：齐次除法（透视除法) 除W
Zndc = - Zclip / Zeye


plt.xlabel('Zeye')
plt.ylabel('Zndc')
plt.plot(Zeye , Zndc )
plt.show()
